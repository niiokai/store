python3 manage.py makemigrations api 
python3 manage.py migrate
python3 manage.py collectstatic --noinput
export DJANGO_SUPERUSER_PASSWORD=$ADMIN_PASSWORD
python3 manage.py createsuperuser --noinput --email $ADMIN_EMAIL