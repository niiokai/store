from django.db import models
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
import uuid
from django.template.defaultfilters import slugify

from decimal import  Decimal
from django.conf import  settings

settings.CART_SESSION_ID = 'dd333442<!!'


#  Get_user_model method will go to Setting Auth_user_model
from django.contrib.auth import get_user_model
User = get_user_model()

def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class BaseModel(models.Model):
    uuid = models.UUIDField(unique=True,default=uuid.uuid4, null=False)
    # id = models.IntegerField(unique=True, primary_key=True)
    slug = models.SlugField(max_length=500, unique=True, blank=True, null=True)
    date_created = models.DateTimeField(blank=True,null=False,auto_now_add=True)
    last_updated = models.DateTimeField(blank=True, null=True, auto_now=True)
    status = models.BooleanField(default=True)

    def save(self, *args,**kwargs ):
        if hasattr( self,'name') and self.name is not None:
            self.slug = slugify(self.name)
        super(BaseModel,self).save(*args,**kwargs)

    class Meta:
        abstract = True


class Brand(BaseModel):

    name = models.CharField(max_length=100, unique=True , null=False)
    image = models.ImageField(upload_to='brand/%Y/%m/%d',blank=True,null=True)

    def  __str__(self):
        return self.name


class Category(BaseModel):

    name = models.CharField(max_length=100, unique=True, null=False)
    image = models.ImageField(upload_to='category/%Y/%m/%d',blank=True,null=True)


    def __str__(self):
        return self.name


class Product(BaseModel):

    name = models.CharField(max_length=100, unique=True , null=False)
    code = models.CharField(max_length=100, unique=True, null=False)
    collection_year = models.PositiveIntegerField(
            default=current_year(), validators=[MinValueValidator(1984),
            max_value_current_year])
    price = models.DecimalField(max_digits=8, decimal_places=2)
    image = models.ImageField(upload_to='product/%Y/%m/%d',blank=True,null=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE,blank=False,
            related_name='products',null=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE,null=False,
                related_name='products', blank=False)
    description = models.CharField(max_length=100, blank=True , null=True)

    def __str__(self):
        return self.name


class ProductImage(BaseModel):
    image = models.ImageField(upload_to='product/%Y/%m/%d',blank=True,null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE,blank=False,
        null=False, related_name='images')

    def __str__(self):
        return self.image


class ProductClip(BaseModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    video_urls = models.CharField(max_length=250, blank=True, )
    image = models.ImageField(upload_to='product/%Y/%m/%d',blank=True,null=True)


class ProductInventory(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE,blank=False,
        null=False)
    quantity_in_stock = models.PositiveIntegerField(default=1)
    #models.ForeignKey(Category, on_delete=models.CASCADE,null=False,
    #blank=False,default=0)
    #batch_number = models.CharField(max_length=250,null=True, blank=True,
    #               unique=True
    reorder_level = models.PositiveIntegerField(default=1)

    #batch_quantity = models.PositiveIntegerField()
    #reference = models.CharField(max_length=50,blank=True, null=True)
    #arrival_date = models.DateTimeField(null=False, auto_now=True,blank=True)


    #collection_year = models.PositiveIntegerField(
    #        default=current_year(), validators=[MinValueValidator(1984),
    #        max_value_current_year])


class Cart(object):

    def __init__(self,request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)

        if not cart :
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart


    def add(self, product, quantity=1, override_quantity=False):

        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart['product_id'] = {'quantity':0, 'price':str(product.price)}

        if  override_quantity:
            self[product_id]['quantity']  = quantity

        else:
            self[product_id]['quantity'] += 1


    def save(self):
        #mark session as modified to make sure it gets saved
        self.session.modified = True


    def remove(self,product_id):
        product_id = str(product_id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    #iterator
    def __iter__(self):

        product_ids = self.cart.keys()
        products =  Products.objects.filter(id__in=product_ids)

        cart = self.cart.copy()

        #get product objects and add them to cart
        for product in products:
            cart[str(product.id)]['product'] = product

        #convert prices to back to decimal
        for item in  cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item


    def __len__(self):
        """
        Count all items in the cart.
        """
        return sum(item['quantity'] for item in self.cart.values())


    def get_total_price(self):
        return sum( Decimal(item['price']) * item['quantity'] )


    def clear(self):
        # remove cart from session
        del self.session[settings.CART_SESSION_ID]
        self.save()


class OrderInfo(BaseModel):
    """
         order information
    """
    ORDER_STATUS = (
        ("SUCCESS", "Success"),
        ("CLOSED", "Timeout close"),
        ("WAIT_BUYER_PAY", "Transaction creation"),
        ("FINISHED", "Transaction end"),
        ("paying", "To be paid"),
    )
    PAY_TYPE = (
        ("momo", "MOMO"),
        ("debit", "DEBIT"),
        ("credit", "CREDIT"),

    )

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="user")
    #Order number
    order_sn = models.CharField("Order number",max_length=30, null=True, blank=True, unique=True)

    #nonce_str = models.CharField("Random encryption",max_length=50, null=True, blank=True, unique=True)

    # Payment gateway transaction number
    trade_no = models.CharField("Transaction number",max_length=100, unique=True, null=True, blank=True)
    #Payment status
    pay_status = models.CharField("Order Status",choices=ORDER_STATUS, default="paying", max_length=30)
    #  Order payment type
    pay_type = models.CharField("Payment Types",choices=PAY_TYPE, default="alipay", max_length=10)
    post_script = models.CharField("Order Message",max_length=200)
    order_amount = models.FloatField("order amount",default=0.0)
    pay_time = models.DateTimeField("Payment time",null=True, blank=True)

    #  User Info
    address = models.CharField("Shipping address",max_length=100, default="")
    signer_name = models.CharField("Sign a person",max_length=20, default="")
    singer_mobile = models.CharField("contact number",max_length=11)


    class Meta:
        verbose_name = "order information"
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.order_sn)


class OrderProductss(BaseModel):
    """
         Product details in the order
    """
    #  One order corresponding to multiple products
    order = models.ForeignKey(OrderInfo, on_delete=models.CASCADE, verbose_name="order information", related_name="goods")
    #  Two foreign key forming a relational table
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="commodity")
    quantity = models.IntegerField("Quantity of goods",default=0)


    class Meta:
        verbose_name = "Ordered products"
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.order.order_sn)


class ShopCart(BaseModel):

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="user")
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                verbose_name="products")
    quantity = models.IntegerField("Purchase quantity",default=0)

    class Meta:
        verbose_name = 'Shopping cart'
        verbose_name_plural = verbose_name
        unique_together = ("user", "product")

    def __str__(self):
        return "%s( %d )".format(self.product.name, self.quantity)



'''



from goods.serializers import GoodsSerializer
from .models import ShoppingCart, OrderGoods, OrderInfo
from rest_framework import serializers
from goods.models import Goods

import time

class OrderViewset(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.DestroyModelMixin,
                   viewsets.GenericViewSet):
    """
         Order management
    list:
                 Get personal orders
    delete:
                 Delete orders
    create：
                 Add an order
    """
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = OrderSerializer

    #  Dynamic configuration Serializer
    def get_serializer_class(self):
        if self.action == "retrieve":
            return OrderDetailSerializer
        return OrderSerializer

    #  Get the list of orders
    def get_queryset(self):
        return OrderInfo.objects.filter(user=self.request.user)

    #  You need to have two steps before the order submit saves, so you can customize the Perform_create method here.
    #  1. Save the goods in the shopping cart in OrderGoods
    #  2. Empty shopping cart
    def perform_create(self, serializer):
        order = serializer.save()
        #  Get all goods in your cart
        shop_carts = ShoppingCart.objects.filter(user=self.request.user)
        for shop_cart in shop_carts:
            order_goods = OrderGoods()
            order_goods.goods = shop_cart.goods
            order_goods.goods_num = shop_cart.nums
            order_goods.order = order
            order_goods.save()
            #  Clear shopping cart
            shop_cart.delete()
        return order
 view.py
'''
