from rest_framework import serializers
from  .models import *
from rest_framework.decorators import action

class BaseModelSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    uuid = serializers.ReadOnlyField()
    slug = serializers.ReadOnlyField()

    class Meta:
            abstract = True


class ProductImageSerializer(BaseModelSerializer):


    class Meta:

        model = ProductImage
        fields = ('id','uuid', 'slug', 'product', 'image')


class ProductClipSerializer(serializers.ModelSerializer):

    class Meta:

        model = ProductClip
        fields = ('id','uuid', 'slug', 'product', 'image','video_urls')



class CategorySerializer(BaseModelSerializer):

    #products = ProductSerializer(many=True, required=False)

    class Meta:
        model = Category
        fields = ('id','uuid', 'slug', 'name', 'image', 'products')


class BrandSerializer(BaseModelSerializer):

    #products = ProductSerializer(many=True, required=False)

    class Meta:

        model = Brand
        fields = ('id','uuid', 'slug', 'name', 'image','products')


class ProductSerializer(BaseModelSerializer):

    #category = serializers.PrimaryKeyRelatedField(
    #        queryset=Category.objects.all()
    #    )

    #brand_url = serializers.HyperlinkedIdentityField(view_name='brand-detail')
    #    category_url = serializers.HyperlinkedIdentityField(
    #                    view_name='category-detail')

    #    category_name = serializers.StringRelatedField()
    #    brand_name = serializers.StringRelatedField()
    #category = serializers.HyperlinkedIdentityField(view_name='category-detail')


    images = ProductImageSerializer(many=True, required=False)

    #brnd = BrandSerializer(required=False)
    #category = CategorySerializer(required=False)

    class Meta:

        model = Product
        fields = ('id','uuid', 'slug', 'name', 'code', 'image', 'description',
         'category','brand', 'collection_year','price', 'images')
        #'brand', 'category',
        #'category_name', 'brand_name',
        #'category_url',
        #'brand_url',
        #depth =  1



    def create(self, validated_data):
        title = validated_data.get('title')
        body = validated_data.get('body')
        slug = slugify(title)
        image = validated_data.get('image', None)

        category_id = validated_data.pop('category')
        brand_id = validated_data.pop('brand')
        images = validated_data.get('image', None)

        if brand_id:
            brand = Brand.objects.get(id=brand_id.id)
        if category_id:
            category = Category.objects.get(id=category_id.id)
            product = Product.objects.create(category=category, brand=brand,
                **validated_data)

        return product


class ProductInventorySerializer(serializers.ModelSerializer):

    class Meta:

        model = ProductInventory
        fields = ('id','uuid', 'slug', 'product','quantity_in_stock',
            'reorder_level')



class ShopCartSerializer(serializers.Serializer):
    #  Get the currently logged in user
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    quantity = serializers.IntegerField(required=False, label="Quantity", min_value=1,
                error_messages={
                    "min_value": "The number of goods cannot be less than one",
                        "required": "Please select the number of purchases"
                })
    product = serializers.PrimaryKeyRelatedField(required=True,
                queryset=Product.objects.all())

    class Meta:
        depth =1

    #  Inherited Serializer has no Save function, you must write a CREATE method
    def create(self, validated_data):
        #  Validated_data is the data that has been processed
        #  Get current users
        #  In View: Self.Request.user; SerizLizer: Self.Context ["Request"]. User
        user = self.context["request"].user
        quantity = validated_data.get("quantity")
        product = validated_data["product"]

        existed = ShopCart.objects.filter(user=user, product=product)
        #  If there is a record in the shopping cart, the quantity is +1
        #  If the shopping car is not recorded, create
        if existed:
            existed = existed[0]
            if  quantity  is not  None:
                existed.quantity += quantity
            existed.save()
        else:
            #  Add to cart
            existed = ShopCart.objects.create(**validated_data)

        return existed


    def update(self, instance, validated_data):
        #  Modify the quantity of items
        instance.quantity = validated_data["quantity"]
        instance.save()
        return instance


class ShopCartDetailSerializer(serializers.ModelSerializer):
    '''
         Shopping Cart Product details
    '''
    #  A shopping cart corresponding to one item
    product = ProductSerializer(many=False, read_only=True)
    class Meta:
        model = ShopCart
        fields = ("product", "quantity")
