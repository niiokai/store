from rest_framework import routers
import api.api_views as views

router = routers.DefaultRouter()

router.register(r'brands', views.BrandViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'products', views.ProductViewSet)
router.register(r'product_images', views.ProductImageViewSet)
router.register(r'product_clips', views.ProductClipViewSet)
router.register(r'inventories', views.ProductInventoryViewSet)

router.register('shopcart', views.ShopCartViewSet)
