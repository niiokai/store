from rest_framework import viewsets
from .serializers import *
from .models import *



class BrandViewSet(viewsets.ModelViewSet):

    queryset =  Brand.objects.all()
    serializer_class = BrandSerializer


class CategoryViewSet(viewsets.ModelViewSet):

    queryset =  Category.objects.all()
    serializer_class = CategorySerializer


class ProductViewSet(viewsets.ModelViewSet):

    queryset =  Product.objects.all()
    serializer_class = ProductSerializer


class ProductImageViewSet(viewsets.ModelViewSet):

    queryset =  ProductImage.objects.all()
    serializer_class = ProductImageSerializer


class ProductClipViewSet(viewsets.ModelViewSet):

    queryset =  ProductClip.objects.all()
    serializer_class = ProductClipSerializer


class ProductInventoryViewSet(viewsets.ModelViewSet):

    queryset =  ProductInventory.objects.all()
    serializer_class = ProductInventorySerializer


'''
class CartViewSet(viewsets.ModelViewSet):

    queryset = Cart.objests.all()
    serializer_class = ShopCartSerializer

'''


class ShopCartViewSet(viewsets.ModelViewSet):

    queryset = ShopCart.objects.all()
    serializer_class = ShopCartSerializer
