from django.contrib import admin
from .models import *

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "uuid", "email", "first_name", "last_name", "role", "date_joined",)
    list_filter = ("is_active", "is_staff", "is_superuser", "date_joined", "modified_date",)
    search_fields = ("email", "first_name", "last_name", "role",)
