FROM python:3

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE 1
RUN mkdir -p /home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/static
RUN mkdir $APP_HOME/media

WORKDIR $APP_HOME

COPY . $APP_HOME

ARG ADMIN_EMAIL
ARG ADMIN_USERNAME
ARG ADMIN_PASSWORD

RUN python3 -m pip install -r $APP_HOME/requirements.txt
EXPOSE 8080

RUN chmod +x $APP_HOME/wait-for-it.sh
RUN chmod +x $APP_HOME/init.sh

CMD [ "./wait-for-it.sh", "postgres:5432", "--", "gunicorn", "--workers=4", "sugarcane.wsgi", "--bind", "0.0.0.0:8080"]